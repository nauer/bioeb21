import os
import requests

from typing import Sequence

def download_uniprot_by_ids(uniprot_ids: Sequence[str], out_dir: str) -> str:
    """
    Schreiben Sie eine Funktion, der sie eine Liste von Uniprot Ids übergeben und die dann über das EBI protein Rest API
    die fasta files in den folder out_dir herunterladen. Geben Sie die Anzahl der geschriebenen Zeichen als Liste
    zurück. Wird ein Id nicht gefunden (Error Statuscode 404), geben Sie für diesen Eintrag None zurück.

    Siehe Rest Beispiel: https://www.ebi.ac.uk/proteins/api/doc/#!/proteins/search

    result = download_uniprot_ids(["P12345", "P12346", "P12", "P12347"], "uniprot")

    print(result)
    [553, 792, None, 265]

    Notes:
        Erzeut für jeden Eintrag ein fasta file in `out_dir`.
        Wenn es `out_dir` noch nicht gibt muss es vorher angelegt werden. Sehen sie sich dafür das Modul os an.

    Args:
        uniprot_ids:
            Eine Liste von Uniprot Ids.
        out_dir:
            Pfad zum Output Folder.

    Returns:
        list
            Gibt jeweils die Anzahl der geschriebenen Zeichen zurück.

    """
    url = "https://www.ebi.ac.uk/proteins/api/proteins/"
    header = {"Accept": "text/x-fasta"}
    protein = "P12345"
    r = requests.get(f"{url}{protein}", headers=header)

    if not r.ok:
        r.raise_for_status()
    else:
        print(r.text)


# Freiwillige extra Aufgabe
# Schreiben sie eine download_uniprot_by_name Funktion, die wie oben funktioniert und nur statt Ids nach dem Namen
# sucht.
# https://www.ebi.ac.uk/proteins/api/proteins?protein=insulin.
#
# Sie werden mehrere Treffer pro suche bekommen, die sie dann runterladen müssen.

if __name__ == "__main__":
    download_uniprot_by_ids([], "")
