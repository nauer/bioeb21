## Webservices
import requests
from time import sleep

## Das Modul requests https://requests.readthedocs.io/en/master/
# https://gitlab.com/api/v4/avatar?email=norbert@derauer.net
r = requests.get(
    "https://gitlab.com/api/v4/avatar", data={"email": "norbert@derauer.net"}
)
r.status_code
r.headers["content-type"]
r.encoding
r.text
'{"type":"User"...'
r.json()

r2 = requests.get(r.json()["avatar_url"])
r2.status_code
r2.text
r2.headers["content-type"]
r2.content

with open("avatar.jpeg", "wb") as f:
    f.write(r2.content)

from PIL import Image
from io import BytesIO

img = Image.open(BytesIO(r2.content))
# get color from x,y in image
img.getpixel((0, 0))
img.getpixel((50, 50))
img.close()


# Write binary content to file
with open("avatar.jpeg", "rb") as f:
    img = Image.open(f)
    print(img.getpixel((10, 40)))
    # f.write(r2.content)


## EBI
base_url = "https://www.ebi.ac.uk/Tools/services/rest/muscle/"

with open("multialignments.fa") as f_in:
    sequence = f_in.read()

data = {
    "sequence": sequence,
    # "format": "fasta",
    "email": "fh@derauer.net",
}

get_run_url = base_url + "run/"
print(get_run_url)

r = requests.post(get_run_url, data=data)
print(r.status_code, r.reason)
job_id = r.text
print(job_id)

# Wait for finishing task
status = ""
while status != "FINISHED":
    print("Job not finished yet")
    sleep(1)
    get_status_url = f"{base_url}status/{job_id}"
    r = requests.get(get_status_url)
    print(r.status_code, r.reason)
    status = r.text
else:
    print("Job finished")

get_result_url = base_url + "resulttypes/" + job_id
r = requests.get(get_result_url)
print(r.status_code, r.reason)
print(r.text)

get_aln_url = base_url + f"result/{job_id}/aln-clustalw"
r = requests.get(get_aln_url)
print(r.status_code, r.reason)
print(r.text)

r = requests.options(get_run_url)
print(r.status_code, r.reason)
print(r.text)
