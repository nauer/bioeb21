from exercise05 import download_uniprot_by_ids

def test_download_uniprot_by_ids(mocker):
    mocker.patch("exercise05.os.path.exists", return_value=True)
    mocker.patch("exercise05.open")

    result = download_uniprot_by_ids(["P12345", "P12346", "P12", "P12347"], 'dir')
    assert result == [553, 792, None, 265]

