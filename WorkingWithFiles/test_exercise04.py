import io
from unittest.mock import mock_open, patch

from exercise04 import read_fasta, word_count


FASTA = """>CCDS22034.1
ATGGCCCTGTGGATGCGCTTCCTGCCCCTGCTGGCCCTGCTCTTCCTCTGGGAGTCCCAC
CCCACCCAGGCTTTTGTCAAGCAGCACCTTTGTGGTTCCCACCTGGTGGAGGCTCTCTAC
CTGGTGTGTGGGGAGCGTGGCTTCTTCTACACACCCATGTCCCGCCGTGAAGTGGAGGAC
CCACAAGTGGCACAACTGGAGCTGGGTGGAGGCCCGGGAGCAGGTGACCTTCAGACCTTG
GCACTGGAGGTGGCCCAGCAGAAGCGTGGCATTGTAGATCAGTGCTGCACCAGCATCTGC
TCCCTCTACCAGCTGGAGAACTACTGCAACTAG"""

PROTEIN = (
    "MALWMRFLPLLALLFLWESHPTQAFVKQHLCGSHLVEALYLVCGER"
    "GFFYTPMSRREVEDPQVAQLELGGGPGAGDLQTLALEVAQQKRGIV"
    "DQCCTSICSLYQLENYCN"
)

WORDS = "abc def\ntest, test,\nole!"


def test_word_count():
    with patch("exercise04.open", mock_open(read_data=WORDS)) as mock_f:
        result = word_count("file_in", "file_out")

    assert mock_f.call_count == 2
    assert result == "file_in 3 5 24"


def test_read_fasta():
    stream_in = io.StringIO(FASTA)
    stream_out = io.StringIO()
    result = read_fasta(stream_in, stream_out)

    assert stream_out.getvalue() == PROTEIN
    assert result == PROTEIN
