import sys

# Write to Stdout
print("Hello World")

# Write to Stdin
print("Error", file=sys.stderr)



# Open a file for writing
file_obj = open("Test.txt", "w")

# Write to file
file_obj.write("Hello World")

# Close file
file_obj.close()



import sys

sys.stdout.write("Hey")
sys.stderr.write("Error")

# Open a file for reading
file_obj = open("Test.txt")

# Read from stream
print(file_obj.read())

# Release resource
file_obj.close()

# Open file in context of open
with open("Test.txt") as file_obj:
    print(file_obj.read())
    print(file_obj.closed)

# File is successfully closed
print(file_obj.closed)



# Open file in context of open
with open("Test.txt") as file_obj:
    print(file_obj.read())
    # Error interrupt execution
    raise ValueError
    print(file_obj.closed)

# But file is still successfully closed
print(file_obj.closed)


def func_raising():
    global file_obj
    file_obj = open("Test.txt")
    print(file_obj.read())
    # Error interrupt execution
    raise ValueError
    # Close file
    file_obj.close()

func_raising()
# File is not closed
print(file_obj.closed)



# Import input/output module
import io

# Simple in-memory text stream
stream = io.StringIO()

print(stream.getvalue())

# Object id of stream
print(id(stream))

# Append a string to stream
stream.write("Hello")

# Still same Object id
print(id(stream))



no_stream = ""

id(no_stream)
no_stream += "Hello"
# Got different id because new string was created
id(no_stream)



import io

def growing_string(repeat = 10):
    string = ""
    for i in range(repeat):
        string += "A" * i
    return len(string)

def growing_stream(repeat = 10):
    stream = io.StringIO()
    for i in range(repeat):
        stream.write("A" * i)
    return len(stream.getvalue())

# %timeit growing_string()
# %timeit growing_stream()


# %timeit growing_string(10000)
# %timeit growing_stream(10000)

from contextlib import contextmanager

@contextmanager
def ctx():
    print('init')
    yield "Tool"
    print('release resources')


with ctx() as val:
    print(f"process {val}")


