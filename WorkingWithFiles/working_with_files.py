# open file
f = open("file.txt")
print(type(f))
print(f.closed)

# close file
f.close()
print(f.closed)

# Open in read mode (default mode)
mode = "r"
f = open("file.txt", mode)

# Read 1 character
print(f.read(1))

# Read another character
print(f.read(1))

# Read a line (from current position to next newline \n)
print(f.readline())

# Read all lines into a list
lines = f.readlines()
print(len(lines))
print(lines[1])

# We are at the end. What happens now?
print(f.read(1))

# Get the position of the file pointer within the file
print(f.tell())

#seek(offset[, whence])
# Back to file beginning
f.seek(0)
print(f.tell())
# offset of 20 from beginning
f.seek(20)
print(f.tell())

# relative to current position
# f.seek(5, 1) # is not supported for Textbuffer
# print(f.tell())

# relative from the file end
# f.seek(-5, 2) # is not supported for Textbuffer
# print(f.tell())

# release the file resource
f.close()

# open in write mode
f_out = open("test.txt", "w")

# Write a string to file
f_out.write("Hello World!")

# Write a list of lines to file
f_out.writelines(["1: A", "2: B"])

f_out.close()

# Streams like TextBuffer are buffered. That means even the write functions returns from the write process that maybe
# not all characters are already in the file.
# f.flush()
# Buffers speed up file processes

# Other possible modes:
# 'r'      Open text file for reading.  The stream is positioned at the
#          beginning of the file.
#
# 'r+'     Open for reading and writing.  The stream is positioned at the
#          beginning of the file.
#
# 'w'      Truncate file to zero length or create text file for writing.
#          The stream is positioned at the beginning of the file.
#
# 'w+'     Open for reading and writing.  The file is created if it does not
#          exist, otherwise it is truncated.  The stream is positioned at
#          the beginning of the file.
#
# 'a'      Open for writing.  The file is created if it does not exist.  The
#          stream is positioned at the end of the file.  Subsequent writes
#          to the file will always end up at the then current end of file,
#          irrespective of any intervening fseek(3) or similar.
#
# 'a+'     Open for reading and writing.  The file is created if it does not
#          exist.  The stream is positioned at the end of the file.  Subse-
#          quent writes to the file will always end up at the then current
#          end of file, irrespective of any intervening fseek(3) or similar.
#
# Additionally there is also a 't' text mode (default) and a 'b' binary mode.

f = open('test.txt', "wb")
b_str = "Da steppt der Bär!".encode()
print(type(b_str), b_str)
f.write(b_str)
f.close()

# Using open in a context manager
with open("file.txt", "r") as f_in:
    print(f_in.read())
    # Close the file automatically with end of block even if an error occurs in the block

# Looping over files
with open("file.txt", "r") as f_in:
    for line in f_in:
        print(len(line))


