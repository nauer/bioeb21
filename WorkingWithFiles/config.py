# JSON
import json
## Load from string
student = json.loads('{ "name":"John", "age":30, "city":"New York"}')

print(type(student))
print(student)
print(student['age'])

## Load JSON from file
with open("address.json", "r") as f_in:
    address = json.load(f_in)

print(type(address))

print(address.keys())

print(address["firstName"])
print(address["phoneNumbers"])

## Save dict as JSON
d = {"name": "John Wick",
     "age": 48,
     "hobbies": ['knit pullovers', 'yoga']}

# ... to string
film_character = json.dumps(d)
print(film_character)

# Pretty print
print(json.dumps(d, indent=4))

# Save to file
with open("characters.json", "w") as f_out:
    json.dump(d, f_out, indent=2, sort_keys=True)

## CSV - Comma-separated values
import csv

# Read csv files
with open('test.csv', newline="") as csvfile:
    reader = csv.reader(csvfile, delimiter=',')

    for row in reader:
        for field in row:
            print(field)

# Write csv files
with open('test2.csv', "w", newline="") as csvfile:
    writer = csv.writer(csvfile, delimiter=';')
    writer.writerow(['Spam'] * 5 + ['Baked Beans'])
    writer.writerow(range(6))

## TOML - similar to ini
# need toml installation
import toml

example = toml.load("example.toml")

print(example)

# write toml to string
print(toml.dumps(d))

# ... and to file
with open("my.toml", "w") as f:
    print(toml.dump(d, f))

