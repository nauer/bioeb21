def word_count(file_in: str, file_out: str) -> str:
    """
    Schreiben Sie ein Programm, dass ein Textfile 'file_in' öffnet, den Text einliest und folgende Informationen in ein
    weiteres File 'file_out' schreibt: Filename, Anzahl der Zeilen, Anzahl der Wörter und Anzahl aller Zeichen.

    test.txt
    Lorem ipsum dolor sit\n
    amet, consectetur\n
    adipiscing elit.

    result = word_count('test.txt', 'outfile')

    print(result)
    test.txt 3 8 56

    Notes:
        Erzeut outfile mit selben Inhalt wie return Wert als Nebeneffekt.

    outfile
    test.txt 3 8 56

    Args:
        file_in:
            Filename vom Input File
        file_out:
            Filename vom Output File

    Returns:
        str

    """


def read_fasta(file_in: io.IOBase, file_out: io.IOBase) -> str:
    """
    Liest eine DNA Fasta Sequenz von einem Stream ein und übersetzt die DNA Sequenz in eine Protein Sequenz und schreibt
    sie in den Output Stream und gibt sie auch als Rückgabewert zurück. Achten sie darauf das Stoppcodone (*) nicht mit
    ausgegeben werden.

    Notes:
        Schreibt result auch in den Out-Stream.
        Um das parsen zu erleichtern ist nur eine Sequenz im Fasta File. Siehe file CCDS22034.1.fa

    CCDS22034.1.fa
    >CCDS22034.1
    ATGGCCCTG
    TGGATGCGC
    TTCCTGCCC

    with open(CCDS22034.1.fa, "r") as f_in:
        with open(file_out, "w") as f_out:
            result = read_fasta(f_in, f_out)
    print(result)

    MALWMRFLP

    Args:
        file_in:
            Stream Objekt z.B: open("File", "r")
        file_out:
            Stream Objekt z.B: open("File", "w")

    Returns:
        str

    """

# Freiwillige extra Aufgabe
# Schreiben sie eine read_fastas Funktion, die mehrere DNA Sequenzen aus dem Fasta File (multi_fasta.fa) lesen kann und
# eine Liste der Proteinsequenzen zurückgibt.