"""
Sequence Library
"""

# Codon tables
codon_table = {
    "TTT": "F",
    "TTC": "F",
    "TTA": "L",
    "TTG": "L",
    "TCT": "S",
    "TCC": "S",
    "TCA": "S",
    "TCG": "S",
    "TAT": "Y",
    "TAC": "Y",
    "TAA": "*",
    "TAG": "*",
    "TGT": "C",
    "TGC": "C",
    "TGA": "*",
    "TGG": "W",
    "CTT": "L",
    "CTC": "L",
    "CTA": "L",
    "CTG": "L",
    "CCT": "P",
    "CCC": "P",
    "CCA": "P",
    "CCG": "P",
    "CAT": "H",
    "CAC": "H",
    "CAA": "Q",
    "CAG": "Q",
    "CGT": "R",
    "CGC": "R",
    "CGA": "R",
    "CGG": "R",
    "ATT": "I",
    "ATC": "I",
    "ATA": "I",
    "ATG": "M",
    "ACT": "T",
    "ACC": "T",
    "ACA": "T",
    "ACG": "T",
    "AAT": "N",
    "AAC": "N",
    "AAA": "K",
    "AAG": "K",
    "AGT": "S",
    "AGC": "S",
    "AGA": "R",
    "AGG": "R",
    "GTT": "V",
    "GTC": "V",
    "GTA": "V",
    "GTG": "V",
    "GCT": "A",
    "GCC": "A",
    "GCA": "A",
    "GCG": "A",
    "GAT": "D",
    "GAC": "D",
    "GAA": "E",
    "GAG": "E",
    "GGT": "G",
    "GGC": "G",
    "GGA": "G",
    "GGG": "G",
}

# Amino acids 3 letter to 1 letter lookup table
amino_acids_3to1 = {
    "CYS": "C",
    "ASP": "D",
    "SER": "S",
    "GLN": "Q",
    "LYS": "K",
    "ILE": "I",
    "PRO": "P",
    "THR": "T",
    "PHE": "F",
    "ASN": "N",
    "GLY": "G",
    "HIS": "H",
    "LEU": "L",
    "ARG": "R",
    "TRP": "W",
    "ALA": "A",
    "VAL": "V",
    "GLU": "E",
    "TYR": "Y",
    "MET": "M",
}

amino_acids_1to3 = dict(zip(amino_acids_3to1.values(), amino_acids_3to1.keys()))


# Functions
def get_content(sequence: str, letter: str) -> int:
    """
    Calculates the ratio of letter frequency in sequence.

    :param sequence:
        Any kind of biological sequence. (DNA, RNA, Peptide)
    :param letter:
        A letter of the same alphabet as sequence.
    :return:
        Percentage of the letter frequency in sequence.

    """
    count = sequence.count(letter)
    return count / len(sequence) * 100


def get_transcription(dna_sequence: str) -> str:
    """
    Calculates the transcript of dna_sequence.

    :param dna_sequence:
        A DNA sequence.
    :return:
        Returns the complement RNA sequence

    """
    # Because maketrans does not depend on an string instance you can call it over the class itself like a class method.
    # But you can also use a str instance e.g. "Any String".maketrans("S", "s")
    table = str.maketrans("ATGC", "UACG")
    return dna_sequence.translate(table)


def get_translation(rna_sequence: str) -> str:
    """
    Translate a RNA sequence to protein sequence.

    Notes:
        Cut sequence to last full codon triplet.

    :param rna_sequence:
        A Sequences sequence.

    :return:
        A protein or peptide sequence.

    """
