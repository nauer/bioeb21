from sequence import get_content, get_transcription, get_translation


def test_get_content():
    assert get_content("ATGC", "C") == 25
    assert get_content("AAAA", "C") == 0
    assert get_content("AAAA", "A") == 100


def test_get_content_type():
    """Test if return type is from type float. """
    assert isinstance(get_content("ATGC", "C"), float)


def test_get_transcription():
    assert get_transcription("ATCGG") == "UAGCC"


def test_get_translation():
    assert get_translation("AUG") == "M"
    assert get_translation("AU") == ""
