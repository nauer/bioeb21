# from sequence import *
from sequence import amino_acids_1to3
from sequence import amino_acids_3to1
from sequence import codon_table
from sequence import get_content, get_transcription, get_translation

# Sequences Cyclus
DNA_1 = "ATGTACCTATCC"
DNA_2 = "TACATGGATAGG"

RNA = "UACAUGGAUAGG"

# complementary Sequences
cDNA = "ATGTACCTATCC"

peptide = "MYLS"

# Playing around with sequences
## Get datatyp
print(type(DNA_1))

## How long is it?
print(len(DNA_1))
print(len(peptide))

## Lower/upper case
print(cDNA.lower())
print(cDNA.upper())

## Get first index of "A"
print(cDNA.index("A"))
## Get first index of "A" starting from index 2
print(cDNA.index("A", 2))

## Count Cs and Gs
count_C = cDNA.count("C")
count_G = cDNA.count("G")
print(count_C, count_G)

## Calculate GC ratio
GC_ratio = (count_C + count_G) / len(cDNA) * 100
print(GC_ratio, "%")

# How to subset strings or lists
## Get first codon
print("first codon: ", cDNA[0:3])
print("first codon: ", cDNA[:3])

## Remember 0 is first index - range is [0,3[ - first index is included last is excluded
# ATGTAC
# [  [
# 012345

## Get second codon
print("second codon: ", cDNA[3:6])

## Get last Codon
print("last codon: ", cDNA[-3:])


# Lookup Tables with dictionaries
## Codon sun - 3 Nucleotides code 1 Aminoacid
print(codon_table)

## Aminoacid 3 letter code to 1 letter
print(amino_acids_3to1)
## Vice versa
print(amino_acids_1to3)

## Functions from sequence library
print(get_content("ATG", "A"))

RNA = get_transcription(DNA_1)
peptide = get_translation(RNA)

print(RNA)
print(peptide)



print(get_content("TTTTGGGG","A"))
print(get_content("TAAGGGG","A"))
print(get_content("TTTTGGGG","T"))

DNA_1 = "ATGTACCTATCC"
print(get_transcription(DNA_1))

RNA = "UACAUGGAUAGG"
print(RNA)