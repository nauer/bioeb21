def div(a, b):
    try:
        a / b
    except Exception:
        print("Division failed")


div(5, 0)
div("5", 2)


def div(a, b):
    try:
        a / b
    except Exception:
        print("Unknown Error")
    except ZeroDivisionError:
        print("Division failed")
    except TypeError:
        print("Arguments have to be from type numeric")



div(5, 0)
div("5", 2)


def div(a, b):
    try:
        a / b
    except ZeroDivisionError as e:
        print("Division failed: ", e)
    except TypeError as e:
        print("Arguments have to be from type numeric: ", e)
    except Exception as e:
        print("Unknown Error: ", e)


div(5, 0)
div("5", 2)


def div(a, b):
    return a / b


def higher_level_function(val1, val2):
    try:
        return div(val1, val2) + div(val2, val1)
    except ZeroDivisionError as e:
        print("Division failed: ", e)
    except TypeError as e:
        print("Arguments have to be from type numeric: ", e)


higher_level_function(5, 2)


def div(a, b):
    try:
        print("Open an expensive database resource")
        a / b
        print("Close database resource")
        b / a
    except Exception as e:
        print("Unknown Error: ", e)
        print("Close database resource")

# Database is not closed if an error occur
div(0, 5)


def div(a, b):
    try:
        print("Open an expensive database resource")
        a / b
    except Exception as e:
        print("Unknown Error: ", e)
    finally:
        print("Close database resource")

# Database is closed even if an error occur
div(5, 0)

def div(a, b):
    try:
        print("Open an expensive database resource")
        a / b
    except Exception as e:
        print("Unknown Error: ", e)

    else:
        print("All done without errors!")
    finally:
        print("Close database resource")

# Database is closed even if an error occur
div(5, 0)


def validate_input(a):
    if not isinstance(a, str):
        raise TypeError("A must be from type str")

validate_input(10)