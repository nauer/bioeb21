# Fragen
* Was ist Tuple unpacking?
* Was macht die `else` Klausel in einer `for` oder `while` Schleife?
* Was macht die built-in Funktion `enumerate`?
* Was ist der Unterschied zwischen einer `for` und einer `while` Schleife
* Was bedeutet `continue` und `break`
* Wofür braucht man `raise`?
* Warum ist Ordnung von `except` Klauseln wichtig?
* Wofür kann man das `finally` Schlüsselwort verwenden?
* Was ist der Unterschied zwischen `/` und `//` bei der Integerdivision?
* Wann wird ein beliebiger Datentyp (list, dict, int, float, set, str, ...) in
  einem Ausdruck als `False` und wann als `True` gewertet. ZB. bool(-4.1)
* Was sind Kommentare und Docstrings
* Was bedeutet zum Beispiel ein Slice [:-4:3]
* Was ist der Unterschied zwischen **mutable** und **immutable**?
* Was ist der Unterschied zwischen `==` und `=`?
* Wie können Sie Zufallszahlen in Python erzeugen?
* Was ist ein **Seed** in Zusammenhang mit Zufallszahlen?
* Was sind die Vor- und Nachteile von Streams gegenüber von Strings (`str`)?
* Wozu kann man Kontextmanager verwenden?
* Was garantieren Kontextmanager?
* Was ist der Unterschied zwischen `return` und `yield`?
* Woran erkennt eine `for` Schleife das ein Generator zu Ende ist?
* Wie kann man aus einem Generator einen Kontextmanager bauen?
* Was ist der große Vorteil von Generatoren?
* Was leitet ein `:` am Ende einer Zeile immer ein?
* Was ist ein *Shebang*? Wozu braucht man ihn in Python?
* Was ist der Unterschied zwischen `Syntax` und `Semantik`?
