# Lecture 3 - Operatoren und Expressionen
## Press Alt+Shift+E to execute line in Python Console
a = 10
b = 5

a < b  # False
a > b  # True
a == b  # False
a != b  # True
a < a  # False
a <= a  # True
type(a != a)  # bool


True and True  # True
True and False  # False
False and False  # False

True or True  # True
True or False  # True
False or False  # False

not True  # False
not False  # True


False and True or True and True

# Is similar to...
(((False and True) or True) and True)

False or True and True

True and True

True


False and True or not True and True

(((False and True) or (not True)) and True)


# Two Functions either returning True or False and prints msg
## For multiline expressions mark all lines and press Alt+Shift+E
def T(msg):
    print(msg, end=" ")
    return True


def F(msg):
    print(msg, end=" ")
    return False


T(1) and T(2) or F(3)  # 1 2  True
F(1) and T(2) or F(3)  # 1 3  False
T(1) or T(2) and F(3)  # 1    True
# It does not matter what the result of T(2) and F(3)
# is because T(1) or ... is always true


T(1) and T(2) and F(3)    # False  1 2 3
F(1) and T(2) and F(3)    # False  1
T(1) and F(2) and F(3)    # False  1 2
F(1) and T(2) or F(3)     # False  1 3
T(1) and F(2) and T(3)    # False  1 2


from random import randint

def init_app1():
  print("App1 has been executed")
  return bool(randint(0,1))

def init_app2():
  print("App2 has been executed")
  return bool(randint(0,1))

# Depend on init_app1() if init_app2() will run
## Run few times
all_succeeded = init_app1() and init_app2()


# Better
app1_exit_code = init_app1()  # init_app1 is started
app2_exit_code = init_app2()  # init_app2 is started

# Same resulat as above
## Run few times
all_succeeded = app1_exit_code and app2_exit_code


# Lecture 3 - Fallunterscheidungen if..elif..else
if False:
    print("A")
elif True:
    print("B")
else:
    print("C")


x = 4

if x < 4:
    print("A")
elif x > 4:
    print("B")
else:
    print("C")


if x <= 4:
    print("A")

if x >= 4:
    print("B")
else:
    print("C")


x = 30
# x = 10

print('yes' if x == 10 else 'no')
