import pandas
import matplotlib
import numpy
import skimage
import sklearn

# Test new Python3.8 feature
if (n := len(['a', 'b'])) > 10:
   print(f"List is too long ({n} elements, expected <= 10)")


def simple_div(a, b):
    """
    Divde a by b. If b is 0 function return None

    :param a: int
    :param b: int
    :return:  float
    """
    if b == 0:
        return None
    return a / b
