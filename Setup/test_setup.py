import setup_test


def test_simple_div():
    assert setup_test.simple_div(5, 0) is None
    assert setup_test.simple_div(5, 2) == 2.5
