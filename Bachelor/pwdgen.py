"""
Create a password generator named "pwdgen.py".

* The command line program generates random passwords
* Password length is variable
* Password character set is variable
* Has a character exclude option
* Has a character must have option (prefix, suffix or within)
* Program should detect invalid options
* The pwdgen should check a password if the fulfill a given policy
* The pwdgen should check if a password is already in a list of weak passwords
  e.g. ["1234", "password"]
* A policy could be that a password need a minimum of N digits, lower case
  characters, upper case characters or special characters
"""