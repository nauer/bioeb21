# Alt + Shift + E
variable_name = "Value"

print(variable_name)

# Löscht Referenz auf den String
del variable_name

# Funktioniert nicht mehr, da Referenz nicht mehr existiert
# print(variable_name)


# Variable name in lower case
size = 10

# Words are joined by _
window_size = 200

# Same for functions
def resize_window():
   pass


# Abbreviation in upper case
colorRGB = "#FF0000"

# Constants in upper case
TIME_OFFSET = 100

# Class names in CamelCase
class Vector3D:
    pass


# int
a = 10

# float
b = 10.
c = .4
d = 10e-4

# bool
wahr = True
falsch = False

# Complex number
complex = 5 + 4j


# str
s1 = "Hallo\nWelt"
s2 = 'Hallo\nWelt'
s3 = "Hallo\\nWelt"  # maskieren
s4 = r"Hallo\nWelt"
s5 = f"Hallo {a} Welt"
s6 = "Hallo {} Welt {}".format(a, c)

print(s1, s2, s3, s4, s5, s6)


# Unicode support since Python 3.1
# See https://unicode-table.com for unicode characters
delta_omega_psi = "\u0394\u03a9\u03a8"

delta_omega_psi

# Even for variable names (Ctrl+Shift+U number)

weather = '\u2602 \u2601 \u2600 \u2603'


a = 5
b = 4
c = a + b + 3
7 / 2               # 3.5 | !In Python 2 {7 / 2 = 3}
3 ** 3              # 27 Exponent 3^3
7 // 2              # 3 Integer Division
7 % 2               # 1 Modulo Division
divmod(7, 2)        # Integer + Modulo Division
abs(2)              # Absolut Wert
a.bit_length()      # 101 -> 3
(255).bit_length()  # 11111111 -> 8


0b101          # Binär Eingabe
0xFF           # Hexadezimal Eingabe
0o10           # Oktadezimal Eingabe
int("10", 5)   # 10 zur Basis 5
int("100", 5)  # 100 zur Basis 5
int("Z", 36)   # max Basis 36

bin(10)
oct(10)
hex(10)

type(10)      # Gibt den Typ int zurück


a = 5.2
b = 4e-4
3 * 0.1
type(10.0)


a = True
b = False

4 == 5
4 < 5

bool([])
bool([1, 3])
bool(10)
bool(-10)    # Achtung - negative Werte ergeben auch True
bool(0)


s0 = "Hallo Welt"
s1 = 'Hallo "Welt"'

s2 = """Geht über
mehrere
Zeilen"""

s3 = '''Diese
auch '''


s4 = "Hallo" + " " + "Welt"
s5 = "Hallo" " " "Welt"

# Vergleiche s2 und s6
s6 = "Dieser Text geht" \
     " auch über mehrere" \
     " Zeilen"

print(s2)
print(s6)

type(s0)


b0 = b"Hallo Welt"
b1 = b'Hallo "Welt"'

b4 = b"Hallo" + b" " + b"Welt"
b5 = b"Hallo" b" " b"Welt"

b6 = b"Dieser Text geht " \
     b"auch ueber mehrere " \
     b"Zeilen"

# b7 = 'Bär'


type(b0)


s0 += s0 + "!"
s0 * 2

DNAseq = "TTGCTAG"
DNAseq.lower()

RNAseq = DNAseq.replace("T", "U")
RNAseq.count("U")
len(DNAseq)
DNAseq.find("TAG")


l = list()              # Erzeugt eine leere Liste
l = []                  # Wie darüber

a = [1, 2, "H", 2, 4.1] # Erzeugt eine Liste mit 5 Feldern
l.append(2)             # Hängt den Integer 2 ans Ende der Liste
l.extend(a)             # Erweitern Liste l mit Liste a
l.append(a)             # Hängt die Liste a als neues Feld ans
                        # Ende von Liste l (eine "nested" List)

l.count(2)              # Gibt an wie oft das Element 2 vorkommt
l.pop()                 # Gibt den letzten Wert zurück und
                        # löscht in aus der Liste
l.pop(1)                # Wie darüber nur mit Index 1
l.insert(1, "Gimli")    # Fügt an Index 1 Gimli in die Liste ein
l.reverse()             # Kehrt die Reihenfolge von l um
l.clear()               # Leert die Liste


l = ['Hallo', 'Welt', '!', 1, 2, 3, '!']
l.index('!')    # Gibt den ersten Index des Strings '!' zurück
l.index('!', 3) # Gibt den Index des Strings '!' ab Index 3 zurück
l[1]            # Gibt den Wert vom Index 1 zurück
l[0:3]          # Erzeugt eine neue Subliste mit dem Bereich 0 - 2
l[0:5:2]        # Erzeugt eine neue SubListe mit jedem 2. Feld
l[-1]           # Gibt das letzte Feld zurück
l[-1:-6]        # Leere Liste - entspricht [6:1]
l[-6:-1]        # entspricht [1:6]
l[-6:]          # Gibt 'Welt' bis Ende des Arrays zurück
l[::-1]         # Selber Effekt wie reverse() - Erzeugt aber Kopie


#  +---+---+---+---+---+
#  | H | i | l | f | e |
#  +---+---+---+---+---+
#  0   1   2   3   4   5
# -5  -4  -3  -2  -1

"Hilfe"[2:4]   # 2 included 4 excluded -> lf
"Hilfe"[:]
"Hilfe"[:3]
"Hilfe"[3:]
"Hilfe"[3:100]
"Hilfe"[::2]   # Step = 2 -> 0 2 4


t = tuple()              # Erzeugt ein leeres Tuple
a = (1, 2, "Hallo", 4.5) # Erzeugt ein Tuple mit 4 Feldern
single = (4, )           # Der , ist nötig
type((4))
type((4,))

# Tuples können nicht erweitert werden
# t.append(2) # will fail
# t.extend(a) # will fail