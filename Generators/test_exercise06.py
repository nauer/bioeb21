import random
import pytest

from unittest.mock import mock_open, patch

from exercise06 import draw_a_card, iterate_fasta, get_new_card_deck

FASTA = b""">CCDS22034.1
ATGGCCCTGTGGAT
GCGCTTCCTGCCCC
TGCT
>Seq2
GGCCCTGCTCTTCCTCTGGGAGTCCCAC
>Seq2
CCCAC
CTGGT"""


@pytest.fixture()
def rand_fixture():
    random.seed(10)
    yield
    random.seed(None)


def test_draw_a_card(rand_fixture):
    gen = draw_a_card(get_new_card_deck())

    assert next(gen) == {"symbol": "spades", "value": "Knave"}
    assert next(gen) == {"symbol": "hearts", "value": "King"}
    assert next(gen) == {"symbol": "diamonds", "value": "3"}
    assert len(list(gen)) == 49


def test_iterate_fasta():
    with patch("exercise06.open", mock_open(read_data=FASTA)):
        gen = iterate_fasta("mock.fa")

        assert next(gen) == {
            "header": b"CCDS22034.1",
            "seq": b"ATGGCCCTGTGGATGCGCTTCCTGCCCCTGCT",
        }
        assert next(gen) == {"header": b"Seq2", "seq": b"GGCCCTGCTCTTCCTCTGGGAGTCCCAC"}
        assert next(gen) == {"header": b"Seq2", "seq": b"CCCACCTGGT"}
