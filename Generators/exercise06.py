import random
from typing import Generator

symbols = {
    "clubs": "\u2660",
    "spades": "\u2663",
    "hearts": "\u2665",
    "diamonds": "\u2666",
}
values = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "Knave", "Queen", "King", "Ass"]
values = dict(zip(values, range(2, 15)))


def get_new_card_deck() -> list:
    """Create a new card deck with 52 cards."""
    deck = []
    for symbol in symbols:
        for value in values:
            deck.append({"symbol": symbol, "value": value})
    return deck


def draw_a_card(card_deck: list) -> Generator:
    """Zieht eine zufällige Karte von `card_deck` und gibt sie zurück.

    Schreiben sie eine Generator-Funktion, der ein Kartenspiel übergeben wird, dieses mischt und eine Karte nach der
    anderen zurückgibt. Für das zufällige mischen schauen sie sich die Funktion shuffle vom Modul random an. Das
    übergebene Kartendeck soll vom Typ sein wie es get_new_card_deck() zurück gibt.

    for card in draw_a_card(get_new_card_deck()):
        print(symbols[card['symbol']], card['value'])

    ->
    ♥ Knave
    ♦ 8
    ♦ Ass
    ♣ Queen
    ...

    Args:
        card_deck:
            Ein Kartendeck

    Yields:
        Eine Karte von einem zufällig gemischten Kartendeck.

    """



def read_fastas(file_name: str) -> list:
    """Öffnet ein File `file_name` im fasta Format und gibt eine Liste von allen Sequenzen als dict im Format

    {'header': "Seq1",
     'sequence': "ATTGC"}

    zurück.

    Args:
        file_name: Filename vom Fasta File

    Returns:
        Liste aller Sequenzen aus `file_name`.

    """
    sequence = b""
    header = b""
    sequences = []
    with open(file_name, "rb") as f_in:
        for line in f_in:
            if line.startswith(b">"):
                if sequence:
                    sequences.append({"header": header, "seq": sequence})
                    sequence = b""
                header = line.strip()[1:]
            else:
                sequence += line.strip()
        else:
            sequences.append({"header": header, "seq": sequence})
    return sequences


def iterate_fasta(file_name: str) -> Generator:
    """Generator Funktion die ein Fasta File `file_name` ausliest und Sequenz für Sequenz zurück gibt.

    Schreiben sie die read_fastas() Funktion zu einem Generator um, dass

    for sequence in iterate_fasta("multi_fasta.fa"):
        print(sequence)

    ->
    {'header': b'CCDS22034.1',
     'seq': b'ATGGCCCTGT...')}

    {'header': b'CCDS22034.2',
     'seq': b'ATGGCCCGAT...')}

    ...

    funktioniert.

    Args:
        file_name: Fasta File

    Yields:
        dict mit fasta Sequenz

    """


# Put your test code into "if __name__ ..." block
if __name__ == "__main__":
    for card in get_new_card_deck():
        print(
            f"{symbols[card['symbol']]}: {card['value']:>6} - {values[card['value']]:>2}"
        )

    for seq in read_fastas("multi_fasta.fa"):
        print(seq)

    # for card in draw_a_card(get_new_card_deck()):
    #     print(symbols[card['symbol']], card['value'])
    #
    # for seq in iterate_fasta("multi_fasta.fa"):
    #     print(seq)
