import sys

# Generator
def get_names():
    print("Before")
    yield "Tom"
    print("After")


gen = get_names()
print(type(gen))

# Funktion
def get_names():
    print("Before")
    return "Tom"
    print("After")


gen = get_names()
print(type(gen))

#
def get_names():
    print("Before")
    yield "Tom"
    print("After")


gen = get_names()
print(next(gen))
# print(next(gen))

#
def get_names():
    print("Before")
    yield "Tom"
    print("After")


for name in get_names():
    print(name)

#
def get_names():
    yield "Jakob"
    yield "Tom"
    print("Und jetzt die Mädchen")
    yield "Marie"


for name in get_names():
    print(name)

#
def get_names(girls_too=True):
    yield "Jakob"
    yield "Tom"

    if not girls_too:
        return  # Close generator

    print("Und jetzt die Mädchen")
    yield "Marie"


for name in get_names(False):
    print(name)

#
# Sub-generator boy names
def get_boy_names():
    names = [
        "Markus",
        "Michael",
        "Peter",
        "Andreas",
        "Norbert",
        "Jakob",
        "Laurenz",
        "Nils",
        "Patrick",
        "Erwin",
    ]
    for name in names:
        yield name


# Sub-generator girl names
def get_girl_names():
    names = [
        "Maria",
        "Petra",
        "Doris",
        "Bettina",
        "Hannah",
        "Pia",
        "Sabine",
        "Paula",
        "Isabella",
        "Ute",
    ]
    for name in names:
        yield name


# Main generator use sub-generators
def get_names():
    yield from get_boy_names()
    yield from get_girl_names()


for name in get_names():
    print(name)

# my_range_nogen returns the complete list of all indices
# All indices are in memory
def my_range_nogen(sequence):
    result = []
    for index, _ in enumerate(sequence):
        result.append(index)
    return result


for i in my_range_nogen(["A", "B", "C"]):
    print(i)


# my_range returns a generator
# Only one index is in memory for each iteration
def my_range(sequence):
    for index, _ in enumerate(sequence):
        yield index


for i in my_range(["A", "B", "C"]):
    print(i)

#
print(sys.getsizeof(my_range_nogen([5] * 100000)))
print(sys.getsizeof(my_range([5] * 100000)))

# Kontextmanager
from contextlib import contextmanager


@contextmanager
def my_ctx():
    # __enter__
    print("Open Context - run before")
    yield
    # __exit__
    print("Close Context - run after")


with my_ctx():
    print("Do some context dependant stuff")


#
from datetime import datetime


@contextmanager
def my_ctx():
    # __enter__
    print("Open Context - run before")
    yield datetime.now()
    # __exit__
    print("Close Context - run after")


with my_ctx() as date:
    print(date, " | Do some context dependant stuff")
