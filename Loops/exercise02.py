"""
* Create a folder exercises in you Gitlab Account
* Copy this file into the exercises folder and finish exercise
* Run pytest for the file. If all tests pass exercise is finished
* Add file or changes to the local git repository 'git add exercise02.py'
* Commit your changes 'git commit'
* Upload new commit to remote repository 'git push'

"""


def stairs(iterations: int, char: str) -> str:
    """
    Create a function printing a stair of characters `char`. Increase count of characters by 1 per line. Start with the
    line number. 0 or negative value for `iterations` returns an empty string.

    result = stairs(4, '*')
    print(result)

    1 *
    2 **
    3 ***
    4 ****

    Args:
        iterations:
           Count of lines. In each line character count is increased by 1.
        character:
           Character what should be used to draw the line.

    Returns:
        str
    """


def toss_the_dice(sum: int = 6) -> int:
    """
    Toss 2 random dices (1-6) as long the sum of both is equal `sum` for 2 consecutive times. Use the randint() function
    from the random module to get the random numbers for the 2 dices. Print out the 2 dices and the sum of them in each
    iteration. If the correct sum is thrown twice in 2 consecutive rounds return how many tries it have taken. Set 6 as
    default value for sum.

    Print out and return the error message "'sum' must be in range of 2-12" if sum is not in range and exit.

    result = toss_the_dice()
    1 5 6
    3 4 7
    1 2 3
    3 3 6
    2 4 6

    print(result)

    5

    Args:
        sum:
            Desired sum of dices.

    Returns:
        int
    """


def index_sequence(sequence: list):
    """
    The function takes a list of strings and return a list of tuples starting with the index, the string and string
    length of the current string. Use enumerate to get the index.

    result = index_sequence(['Test','Hello World!", "*"*5])
    print(result)

    [(0 ,'Test', 4), (1, 'Hello World!', 12), (2, '*****',5)]

    Args:
        sequence:
            A list of strings

    Returns:
        list - A list of tuples
    """


# Do not change anything after this line
import pytest
import random


@pytest.mark.parametrize(
    "iter, char, expected",
    [(4, "*", "1 *\n2 **\n3 ***\n4 ****"), (1, "_", "1 _"), (-4, ":", "")],
)
def test_stairs(iter, char, expected):
    assert stairs(iter, char) == expected


def test_toss_the_dice():
    random.seed(10)
    assert toss_the_dice(6) == 4
    assert toss_the_dice(12) == 795
    assert toss_the_dice(13) == "'sum' must be in range of 2-12"
    assert toss_the_dice(1) == "'sum' must be in range of 2-12"
    random.seed(None)


@pytest.mark.parametrize(
    "list, expected",
    [(["A", "BB"], [(0, "A", 1), (1, "BB", 2)]), (["Test"], [(0, "Test", 4)])],
)
def test_index_sequence(list, expected):
    assert index_sequence(list) == expected
