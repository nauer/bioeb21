"""
* Create a folder exercises in you Gitlab Account
* Copy this file into the exercises folder and finish exercise
* Run pytest for the file. If all tests pass exercise is finished
* Add file or changes to the local git repository 'git add exercise03.py'
* Commit your changes 'git commit'
* Upload new commit to remote repository 'git push'

Guess a Number Game

Description:
Write a script that asks the user to guess a number between 1 and 10. The script randomly set a number between 1 and 10.
If the user guesses the correct number the script writes "You got it" to the terminal. If the value is to small the
script writes "Too small you have X tries left". If the value is to high the scripts writes "Too high you have X tries
left". If the user guess is not in the range between 1 and 10 the user should repeat the input with "Number is not in
range. Guess a number between 1 and 10". This does not count a try. The user have 3 tries to guess the correct number.
If the user fails print "You lost the game!"

* First think about which parts are needed
* Tranlates this parts to functions
* Compile all parts together in a main function

Let's do it step by step.

* main loop (play again, exit) while
* Set random number
* User input
* Define loop
* Compare user input with random number
* User output
* Win, loose


"""
import random


def get_random_number(min_val: int, max_val: int) -> int:
    """
    Function should return a random number between 'min_val' and 'max_val' including both min_val' and 'max_val'.

    Use function randint from the random module - https://docs.python.org/3/library/random.html

    Args:
        min_val:
            Lower limit for the random number.
        max_val:
            Upper limit for the random number.

    Returns:
        A random number in the range of [min_val, max_val].

    """


def process_user_input(min_val: int, max_val: int) -> int:
    """
    Convert user input string to integer.

    Use input() for the user request.
    Repeat as long if value is in range.

    Args:
        min_val:
            Lower limit for the number to guess.
        max_val:
            Upper limit for the number to guess.

    Returns:
        Guessed number.

    """


def iterate_guesses(to_guess: int, retries: int = 3) -> bool:
    """
    Loop over all retries.

    Return True if user guess the correct number. Return False if user can not guess number in 'retries' rounds.

    Args:
        to_guess:
            Number to guess
        retries:
            Allowed re-tries before function stops

    Returns:
        True if guess was successfully. False otherwise.

    """


def guess_a_number() -> None:
    """Put all together in this function."""


# Call the game
if __name__ == "__main__":
    guess_a_number()
