# Create a tuple or list
datum = (10, "Sep", 2017)

# Same as above
datum = 10, "Sep", 2017

# Unpacking
day, month, year = datum


# Switching values
x = 10
y = 20

# Switching variables without unpacking needs an extra variable
temp = x
x = y
y = temp

# With Python packing all done in one command
x, y = (y, x)

# or even shorter
x, y = y, x


# Create a tuple
data = ["vienna_01", 100, 102, 98, 103, 107, "valid"]


# Extract id (vienna_01), samples and if data is valid
sid, *samples = data
sid      # 'vienna_01'
samples  # (100, 102, 98, 103, 107, 'valid')


sid, *samples, is_valid = data
sid       # 'vienna_01'
samples   # (100, 102, 98, 103, 107)
is_valid  # 'valid'


# For loops
for day in ["Montag", "Dienstag", "Mittwoch"]:
    print("Heute ist", day)

for character in "Schifffahrtskapitänsmütze":
    print(character)

# Sequence of integers
for i in range(5):
    print(i)

# Sequence of list items
for i in [5, 3, "nan", 0, 10]:
    print(i)

# Sequence of string characters
for i in "Hello World!":
    print(i)


# While loops
i = 0
while i < 10:
    print(i)

    i += 1

# Endless loop
# i = 0
# i will never reach 10 and run forever
# while i < 10:
#     print(i)

# break & continue
for i in range(10):
    # Modulo division returns remainder 5 % 3 == 2
    if i % 2 == 0:
        continue

    if i == 7:
        break

    print(i)


i = 0
while i < 10:
    print(i)
    i += 1

    if i == 5:
        break

# loops with else
i = 0
while i < 10:
    print(i)
    i += 1
else:
    print("All done")


i = 0
while i < 10:
    print(i)
    i += 1

    if i == 5:
        break
else:
    print("All done")


# enumerate
sequence = ["A", "B", "C"]

enum = enumerate(sequence)

list(enum)  # [(0, 'A'), (1, 'B'), (2,'C')]


my_list = [10, 20, 1, 5, "ende", 1.5, 100, 0]
index = 0
for item in my_list:
    print(index, item)
    index += 1

    if index > 3:
        break


# enumerate returns item number and item
for enum in enumerate(my_list):  # (0, 10) (1, 20) (2, 1)
    print(enum)
    if enum[0] > 3:
        break

# use Tuple unpacking
for index, item in enumerate(my_list):
    print(index, item)
    if index > 3:
        break

# alternative start index
for index, item in enumerate(my_list, start=3):  # (2, 10) (3, 20)
    print(index, item)
    if index > 3:
        break
