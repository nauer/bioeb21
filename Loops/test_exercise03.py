import pytest
from exercise03 import get_random_number, process_user_input, iterate_guesses


@pytest.mark.parametrize(
    "min_val, max_val, exp_output",
    [(1, 10, 5)],
)
def test_get_random_number(min_val, max_val, exp_output, mocker):
    mocker.patch("random.randint", return_value=5)
    assert get_random_number(min_val, max_val) == exp_output


def test_process_user_input(mocker):
    mocker.patch("exercise03.input", return_value=5)
    assert process_user_input(3, 6) == 5


def test_iterate_guesses_loose(mocker):
    mock_user_input = mocker.patch(
        "exercise03.process_user_input", side_effect=[5, 4, 3, 2, 6]
    )
    result = iterate_guesses(1, 5)

    assert mock_user_input.call_count == 5
    assert result is False


def test_iterate_guesses_win(mocker):
    mock_user_input = mocker.patch(
        "exercise03.process_user_input", side_effect=[5, 4, 1]
    )
    result = iterate_guesses(1)

    assert mock_user_input.call_count == 3
    assert result is True
